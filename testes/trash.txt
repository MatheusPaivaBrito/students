@app.route('/add', methods=['GET', 'POST'])
def add():
    if request.method == 'POST':
        student = Estudante(request.form['name'], request.form['birth'])
        db.session.add(student)
        db.session.commit()
        return redirect(url_for('index'))
    return render_template('add.html')

/////////////////////////////////////////////////////////////////

$(document).ready(function(){
    $("#myBtn").click(function(){
        var str = $("#name").val();
        alert(str);
    });
});

////////////////////////////////////////////////////////////////////

<script type="text/javascript">

    var server = "http://127.0.0.1:5000";
    var op_num = {'sum':[4]};

    function update_var(){
        var name = parseFloat($("#name").val());
        op_num['sum']=[name];
    }
    $( function() {
        $("#sum").click(function() {
            var appdir='/sum'
            update_var();

            $.ajax({
                type : "POST",
                url: server+appdir,
                data: JSON.stringify(op_num),
                dataType: 'json'
            }).done(function(data) {
                console.log(data);
            });
        });
    });
</script>
////////////////////////////////////////////////////////////////////////////////////////
<!doctype html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>Add Produto</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script> 
    <script src="static/scripts/add.js"></script>
</head>
<body>
<h1 style="text-align: center;">Registro de Novo Aluno</h1>
<form action="/add" class="container" method="POST">
    <div class="row">
        <div class="col-md-6">
            <input type="text" name="name" id="inputName" placeholder="Nome" class="form-control">
        </div>
        <div class="col-md-6">
            <input type="date" name="birth" id="inputBirth" placeholder="Data de Nascimento" class="form-control">
        </div>
    </div>
    <br>
    <div class="row col-xs-12">
        <button type="submit" class="btn btn-blick btn-success">Salvar</button>
    </div>
</form>
</body>
</html>

////////////////////////////////////////////////////////////////////////////////////

@app.route('/indexAlunos')
def alunos():
    student = Estudante.query.all()
    name = student.name
    birth = student.birth
    return jsonify({'status':'OK', 'name': name , 'birth': birth})

/////////////////////////////////////////////////////////////////////////////////////

<!doctype html>
<html lang="pt-br">
<head>

    <meta charset="UTF-8">

    <meta name="viewport" 
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="icon" type="image/png" href="static/icon/favicon.png"/>

    <script src="/static/js/jquery-3.6.0.js"></script> 
	<script src="/static/js/index.js"></script>

    <title>Alunos</title>

</head>

<body id="bodyIndex" data-student="{{student}}">

<h1 style="text-align: center;">Alunos Cadastrados</h1>

<div class="container">

    <div class="row">

        <div class="col-xs-12">
            <a href="/add" class="btn btn-blick btn-success">Adicionar Aluno</a>
        </div>

        <div class="col-xs-12">

            <table class="table">

                <thead>

                <tr>
                    <th>Matricula:</th>
                    <th>Nome:</th>
                    <th>Data de Nascimento:</th>
                </tr>

                </thead>

                <tbody>
                {% for e in student %}
                <tr>

                    <td id="indexId">{{e.id}}</td>
                    <td id="indexName">{{e.name}}</td>
                    <td id="indexBirth">{{e.birth}}</td>

                    <td>
                        <a href="/grades/{{e.id}}" class="btn btn-blick btn-success">Ver Notas</a>
                        <a href="/delete/{{e.id}}" class="btn btn-blick btn-success">Excluir</a>
                        <a href="/edit/{{e.id}}" class="btn btn-blick btn-success">Editar</a>
                    </td>

                </tr>
                {% endfor %}              
                </tbody>

            </table>

        </div>

    </div>

</div>    

</body>

</html>

//////////////////////////////////////////////////////////////////////////////////////////////////////

<h1 style="text-align: center;">Editar {{student.name}}</h1>

<form action="/edit/{{ student.id }}" class="container" method="POST">

    <div class="row">

        <div class="col-md-6">
            <input type="text" name="name" id="name" placeholder="Nome" class="form-control" value="{{student.name}}">
        </div>

        <div class="col-md-6">
            <input type="date" name="birth" id="birth" placeholder="Data de Nascimento" class="form-control" value="{{student.birth}}">
        </div>

    </div>

    <br>

    <div class="row col-xs-12">
        <button type="submit" class="btn btn-blick btn-success">Salvar</button>
    </div>

</form>

//////////////////////////////////////////////////////////////

student.name = request.form['name']
    student.birth = request.form['birth']
    db.session.commit()

///////////////////////////////////////////////////////////////////////

$(function(){
	$('#editbutton').click(function(){
		var name = $('#inputname').val();
		var birth = $('#inputbirth').val();
		$.ajax({
			url: '/editAluno/id',
			data: {
				name: name,
				birth: birth
			},
			type: 'POST',
			success: function(response){
				console.log(response);
				window.location.href = ('/');
			},
			error: function(error){
				console.log(error);
			}
		});
	});
});

////////////////////////////////////////////////////////

$.ajax({
    url: '/editAluno',
    success: function() {
		for (var x=0; x<data.length; x++) {
		var students = $(
		'<h1 style="text-align: center;">' 
		+
		data[6].id
		+
		'</h1>'
				+
				'<div class="row">'
					+
        			'<div class="col-md-6">'
					+
            			'<input  type="text" name="name" id="inputname" placeholder="Nome" class="form-control" value="">'
					+
        			'</div>'
				+
        '			<div class="col-md-6">'
				+
            			'<input type="date" name="birth" id="inputbirth" placeholder="Data de Nascimento" class="form-control" value="">'
				+
        			'</div>'
				+
    			'</div>'
			+
    		'<br>'
			+
    			'<div class="row col-xs-12">'
			+
        		'<button id="editbutton" type="button" class="btn btn-blick btn-success">' 
				+ 
				"Salvar" 
				+ 
				'</button>'
			+
    			'</div>'
            );

            $('#editform').append(students);
		}

    },
    error: function(jqXHR, textStatus, errorThrown){
        alert('Error: ' + textStatus + ' - ' + errorThrown);
    }
});

///////////////////////////////////////////////////////////////////////

<script>
    function editAluno(){
        window.location.href = ("/edit/" + student[x].id);
    }
</script>

//////////////////////////////////////////////////////////////////////////

db.session.query(Nota).filter(Nota.avarage > 0)

///////////////////////////////////////////////////////////////////////////

<!doctype html>
<html lang="pt-br">
<head>

    <meta charset="UTF-8">

    <meta name="viewport" 
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="icon" type="image/png" href="../static/icon/favicon.png"/>

    <script src="/static/js/jquery-3.6.0.js"></script> 
	<script src="/static/js/grades.js"></script>

    <title>Notas</title>

</head>

<body>

<h1 style="text-align: center;">Notas de {{student.name}}</h1>

<div class="container">

    <div class="row">

        <div class="col-xs-12">
            <a href="/addgrades/{{ student.id }}" class="btn btn-blick btn-success">Adicionar Notas</a>
            <a href="/" class="btn btn-blick btn-success">Retornar a Alunos</a>
            <button type="button" >Maiores Notas</button>
        </div>

        <div class="col-xs-12">
            
            <table class="table">

                <thead>

                <tr>
                    <th>Redação:</th>
                    <th>Humanas:</th>
                    <th>Naturais:</th>
                    <th>Linguagens:</th>
                    <th>Média:</th>
                </tr>

                </thead>

                <tbody>
                {% for n in notes %}
                <tr>

                    <td>{{n.reda|round(1)}}</td>
                    <td>{{n.human|round(1)}}</td>
                    <td>{{n.nat|round(1)}}</td>
                    <td>{{n.ling|round(1)}}</td>
                    <td>{{n.avarage|round(1)}}</td>

                    <td>
                        <a href="/deletegrades/{{n.id}}" class="btn btn-blick btn-success">Excluir</a>
                        <a href="/editgrades/{{n.id}}" class="btn btn-blick btn-success">Editar</a>
                    </td>

                </tr>
                {% endfor %}
                </tbody>

            </table>

        </div>

    </div>

</div> 
  
</body>

</html>