from flask_sqlalchemy import SQLAlchemy
from flask_login import UserMixin
from sqlalchemy import Column, Integer, String

db = SQLAlchemy()

class Usuario(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(20), nullable=False, unique=True)
    password = db.Column(db.String(20), nullable=False)

    def __init__(self, username, password):
        self.username = username
        self.password = password

class Estudante(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(20), nullable=False)
    birth = db.Column(db.Date, nullable=False)
    notas = db.relationship('Nota', backref='owner', lazy='dynamic')

    def __init__(self, name, birth):
        self.name = name
        self.birth = birth

class Nota(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    reda = db.Column(db.Float(4,2), nullable=False)
    human = db.Column(db.Float(4,2), nullable=False)
    nat = db.Column(db.Float(4,2), nullable=False)
    ling = db.Column(db.Float(4,2), nullable=False)
    avarage = db.Column(db.Float(4,2), nullable=False)
    year = db.Column(db.Integer(), nullable=False)
    owner_id = db.Column(db.Integer, db.ForeignKey('estudante.id'))

    def __init__(self, owner_id, reda, human, nat, ling, avarage):
        self.owner_id = owner_id
        self.reda = reda
        self.human = human
        self.nat = nat
        self.ling = ling
        self.year = year
        self.avarage = avarage

class Patrimonio(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name_patrimony = db.Column(db.String(20), nullable=False)
    type_patrimony = db.Column(db.String(20), nullable=False)
    amount_patrimony = db.Column(db.Integer(), nullable=False)
    value_patrimony  = db.Column(db.Float(4,2), nullable=False)

    def __init__(self, name_patrimony, type_patrimony, amount_patrimony, value_patrimony):
        self.name_patrimony = name_patrimony
        self.type_patrimony = type_patrimony
        self.amount_patrimony = amount_patrimony
        self.value_patrimony = value_patrimony