from models import db, Usuario, Estudante, Nota, Patrimonio

from flask import Flask, render_template, request, url_for, redirect, Response, json, jsonify
from flask_login import LoginManager, login_user, login_required, logout_user, current_user
from sqlalchemy import asc, desc
from slackclient import SlackClient

slack_token = "xoxb-2363946617269-2382310328498-fbzFzhcXWqTEwxNeeC4Mddht"
sc = SlackClient(slack_token)

app = Flask(__name__, template_folder='templates')
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:root@localhost:3306/Server'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config['SECRET_KEY'] = 'secret'

login_manager = LoginManager()
login_manager.init_app(app)

@login_manager.user_loader
def get_user(user_id):
    return Usuario.query.filter_by(id=user_id).first()

@app.route('/')
def login():
    return render_template('login.html')

@app.route('/login/js/', methods=['POST'])
def login_js():
    if request.method == 'POST':

        username = request.form['username_login_js']
        password = request.form['password_login_js']

        user = Usuario.query.filter_by(username=username, password = password).first()

        if user is not None and user.username.lower() == username.lower():
            if user is not None and user.password == password:
                login_user(user)
                return jsonify("success")

@app.route('/signup/')
def signup():
    return render_template('signup.html')

@app.route('/signup/js/', methods=['POST'])
def signup_js():
        
    userregistration = Usuario(request.form['username_signup_js'].lower(), request.form['password_signup_js'])

    user = Usuario.query.filter_by(username=userregistration.username).first()

    if user is None:
        db.session.add(userregistration)
        db.session.commit()
        return jsonify()
    else:
        return jsonify("The Username is Taken")

@app.route('/logout/')
def logout():
    logout_user()
    return redirect(url_for('login'))

@app.route('/students/')
@login_required
def students():
    return render_template('students.html')

@app.route('/students/js/')
@login_required
def students_js():
    students = Estudante.query.all()
    studentsserealization = [{"id": x.id, "name": x.name, "birth": x.birth} for x in students]
    return jsonify(studentsserealization)

@app.route('/students/search/')
@login_required
def students_search():
    return render_template('students_search.html')

@app.route('/students/search/js/')
@login_required
def students_search_js():
    students = Estudante.query.all()
    studentsserealization = [{"id": x.id, "name": x.name, "birth": x.birth} for x in students]
    return jsonify(studentsserealization)

@app.route('/students/graphics/')
@login_required
def students_graphics():
    return render_template('students_graphics.html')

@app.route('/students/graphics/js/', methods=['POST'])
@login_required
def students_graphics_js():

    students = Estudante.query.all()
    notes = Nota.query.all()
    
    studentsserealization = [{"id": int(x.id), "name": x.name, "birth": x.birth} for x in students]
    notesserealization = [{"reda": float(x.reda), "human": float(x.human), "nat": float(x.nat), "ling": float(x.ling), "avarage": float(x.avarage), "id": x.id, "owner_id": x.owner_id} for x in notes]

    data = []

    for x in studentsserealization:
        for y in notesserealization:
            if x["id"] == y["owner_id"]:
                dictionary = {
                        "name": x["name"],
                        "avarage": y["avarage"]
                    }
                data.append(dictionary)
                break
    
    return jsonify(data)

@app.route('/students/add/')
@login_required
def students_add():
    return render_template('students_add.html')

@app.route('/students/add/js/', methods=['POST'])
@login_required
def students_add_js():
    student = Estudante(request.form['name_students_js'], request.form['birth_students_js'])

    db.session.add(student)
    db.session.commit()

    sc.api_call(
    "chat.postMessage",
    channel="#estudantes",
    text="O Aluno " + student.name + " Foi Adicionado"
    )

    return jsonify()

@app.route('/students/<int:students_id>/delete/')
@login_required
def students_delete(students_id):
    student = Estudante.query.get(students_id)
    db.session.delete(student)
    db.session.commit()
    return redirect(url_for('students'))

@app.route('/students/<int:students_id>/edit/', methods=['GET', 'POST'])
@login_required
def students_edit(students_id):
    return render_template('students_edit.html')

@app.route('/students/<int:students_id>/edit/js/', methods=['GET', 'POST'])
@login_required
def students_edit_js(students_id):
    student = Estudante.query.get(students_id)
    students = [{"id": student.id, "name": student.name, "birth": student.birth}]
    if request.method == 'POST':
        student.name = request.form['name_students_js']
        student.birth = request.form['birth_students_js']
        db.session.commit()
        return jsonify()
    return jsonify(students)

@app.route('/students/<int:students_id>/grades/', methods=['GET'])
@login_required
def students_grades(students_id):
    return render_template('students_grades.html')

@app.route('/students/<int:students_id>/grades/js/', methods=['GET', 'POST'])
@login_required
def students_grades_js(students_id):
    notes = Nota.query.filter_by(owner_id = students_id).all()
    notesserealization = [{"reda": float(x.reda), "human": float(x.human), "nat": float(x.nat), "ling": float(x.ling), "avarage": float(x.avarage), "id": x.id, "owner_id": x.owner_id} for x in notes]
    return jsonify(notesserealization)

@app.route('/students/<int:students_id>/grades/filter/<int:filter_id>/', methods=['GET'])
@login_required
def students_grades_filter(students_id, filter_id):
    return render_template('students_grades_filter.html')

@app.route('/students/<int:students_id>/grades/filter/<int:filter_id>/js/', methods=['GET', 'POST'])
@login_required
def students_grades_filter_js(students_id, filter_id):
    if filter_id == 1:
        notes = Nota.query.all()
        filternotesserealization = [{"reda": float(x.reda), "human": float(x.human), "nat": float(x.nat), "ling": float(x.ling), "avarage": float(x.avarage), "id": x.id, "owner_id": x.owner_id} for x in notes]
    else:
        if filter_id == 2:
            filtervalue = db.session.query(Nota).order_by(asc(Nota.avarage))
            filternotesserealization = [{"reda": float(x.reda), "human": float(x.human), "nat": float(x.nat), "ling": float(x.ling), "avarage": float(x.avarage), "id": x.id, "owner_id": x.owner_id} for x in filtervalue]
        else:
            if filter_id == 3:
                filtervalue = db.session.query(Nota).order_by(desc(Nota.avarage))
                filternotesserealization = [{"reda": float(x.reda), "human": float(x.human), "nat": float(x.nat), "ling": float(x.ling), "avarage": float(x.avarage), "id": x.id, "owner_id": x.owner_id} for x in filtervalue]
    return jsonify (filternotesserealization)

@app.route('/students/<int:students_id>/grades/add/')
@login_required
def students_grades_add(students_id):
    return render_template('students_grades_add.html')

@app.route('/students/<int:students_id>/grades/add/js/', methods=['POST'])
@login_required
def students_grades_add_js(students_id):
    note = Nota(owner_id=request.form['owner_id_js'], reda=request.form['reda_js'], human=request.form['human_js'], nat=request.form['nat_js'], ling=request.form['ling_js'], avarage=(Nota.reda+Nota.human+Nota.nat+Nota.ling)/4)
    db.session.add(note)
    db.session.commit()
    return jsonify()

@app.route('/students/<int:students_id>/grades/<int:grades_id>/delete/')
@login_required
def students_grades_delete(students_id, grades_id):
    note = Nota.query.get(grades_id)
    db.session.delete(note)
    db.session.commit()
    return render_template('students_grades.html')

@app.route('/students/<int:students_id>/grades/<int:grades_id>/edit/', methods=['GET', 'POST'])
@login_required
def students_grades_edit(students_id, grades_id):
    return render_template('students_grades_edit.html')

@app.route('/students/<int:students_id>/grades/<int:grades_id>/edit/js/', methods=['GET', 'POST'])
@login_required
def students_grades_edit_js(students_id, grades_id):
    notes = Nota.query.get(grades_id)
    notesserealization = [{"reda": float(notes.reda), "human": float(notes.human), "nat": float(notes.nat), "ling": float(notes.ling), "avarage": float(notes.avarage), "id": notes.id, "owner_id": notes.owner_id}]
    if request.method == 'POST':     
        notes.reda = request.form['reda_js']
        notes.human = request.form['human_js']
        notes.nat = request.form['nat_js']
        notes.ling = request.form['ling_js']
        notes.avarage = (Nota.reda+Nota.human+Nota.nat+Nota.ling)/4
        db.session.commit()
        return jsonify()
    return jsonify(notesserealization)

@app.route('/patrimony/')
@login_required
def patrimony():
    return render_template('patrimony.html')

@app.route('/patrimony/js/')
@login_required
def patrimony_js():
    patrimony = Patrimonio.query.all()
    patrimonyserealization = [{"id_patrimony": x.id, "name_patrimony": x.name_patrimony, "type_patrimony": x.type_patrimony, "amount_patrimony": int(x.amount_patrimony), "value_patrimony": float(x.value_patrimony)} for x in patrimony]
    return jsonify(patrimonyserealization)

@app.route('/patrimony/add/')
@login_required
def patrimony_add():
    return render_template('patrimony_add.html')

@app.route('/patrimony/add/js/', methods=['POST'])
@login_required
def patrimony_add_js():
    patrimony = Patrimonio(request.form['name_patrimony_js'], request.form['type_patrimony_js'], request.form['amount_patrimony_js'], request.form['value_patrimony_js'])
    db.session.add(patrimony)
    db.session.commit()
    return jsonify()

@app.route('/patrimony/<int:patrimony_id>/delete/')
@login_required
def patrimony_delete(patrimony_id):
    patrimony = Patrimonio.query.get(id)
    db.session.delete(patrimony)
    db.session.commit()
    return redirect(url_for('patrimony'))

@app.route('/patrimony/<int:patrimony_id>/edit/', methods=['GET', 'POST'])
@login_required
def patrimony_edit(patrimony_id):
    return render_template('patrimony_edit.html')

@app.route('/patrimony/<int:patrimony_id>/edit/js/', methods=['GET', 'POST'])
@login_required
def patrimony_edit_js(patrimony_id):
    patrimony = Patrimonio.query.all()
    patrimonyserealization = [{"id_patrimony": x.id, "name_patrimony": x.name_patrimony, "type_patrimony": x.type_patrimony, "amount_patrimony": int(x.amount_patrimony), "value_patrimony": float(x.value_patrimony)} for x in patrimony]
    if request.method == 'POST':
        patrimonyid = request.form['id_patrimony_js']
        patrimony =  Patrimonio.query.get(patrimonyid)
        patrimony.name_patrimony = request.form['name_patrimony_js']
        patrimony.type_patrimony = request.form['type_patrimony_js']
        patrimony.amount_patrimony = request.form['amount_patrimony_js']
        patrimony.value_patrimony = request.form['value_patrimony_js']
        db.session.commit()
        return jsonify()
    return jsonify(patrimonyserealization)

if __name__ == '__main__':
    db.init_app(app=app)
    with app.test_request_context():
        db.create_all()
    app.run(debug=True)