function changepassworld() {
    var x = document.getElementById("input_password_login");
    if (x.type === "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
  }

  $(function(){
    $('#button_login').click(function(){
      var username_login = $('#input_username_login').val();
      var password_login = $('#input_password_login').val();
      $.ajax({
        url: '/login/js/',
        data: {
          username_login_js: username_login,
          password_login_js: password_login
        },
        type: 'POST',
        success: function(data){
          if (data == "success") {
            window.location.href = ('/students/');
          }
          
        },
        error: function(error){
          alert("Nome de Usuario ou Senha Incorretos")
        }
      });
    });
  });
  