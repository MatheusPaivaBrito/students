const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const url_students_id = urlParams.get('students_id')
const url_grades_id = urlParams.get('grades_id')
const url_name = urlParams.get('name')

$.ajax({
	url: '/students/' + url_students_id + '/grades/'+ url_grades_id +'/edit/js/',
		success: function(data) {
			var student = data.length
				var students = $(
					'<h1 style="text-align: center;">' + "Editar as Notas de " + url_name + '</h1>' 
							+
							'<div class="row">'
								+
								'<div class="col-md-6">'
								+
									'<input id="input_reda" name="reda" type="number" class="form-control" placeholder="Redação" value="' + data[0].reda + '">'
								+
								'</div>'
								+
								'<div class="col-md-6">'
								+
									'<input id="input_human" name="human" type="number" class="form-control" placeholder="Humanas" value="' + data[0].human + '">'
								+
								'</div>'
								+
								'<div class="col-md-6">'
								+
									'<input id="input_nat" name="nat" type="number" class="form-control" placeholder="Naturais" value="' + data[0].nat + '">'
								+
								'</div>'
								+
								'<div class="col-md-6">'
								+
									'<input id="input_ling" name="ling" type="number" class="form-control" placeholder="Linguagens" value="' + data[0].ling + '">'
								+
								'</div>'
								+
							'</div>'
							+
						'<br>'
						+
						'<div class="d-grid gap-2 d-md-block">'
							+
							'<button id="button_edit_grades" type="button" class="btn btn-success">'
								+ 
								'<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-check-lg" viewBox="0 0 20 20">'
									+
									'<path d="M13.485 1.431a1.473 1.473 0 0 1 2.104 2.062l-7.84 9.801a1.473 1.473 0 0 1-2.12.04L.431 8.138a1.473 1.473 0 0 1 2.084-2.083l4.111 4.112 6.82-8.69a.486.486 0 0 1 .04-.045z"/>'
									+
								'</svg>'
								+
								"Salvar"
								+ 
							'</button>'
							+ 
						'</div>'
					);
				$('#form_edit_grades').append(students);				
			},
	error: function(jqXHR, textStatus, errorThrown){
		alert('Error: ' + textStatus + ' - ' + errorThrown);
	}
});

$(function(){
	$('#button_edit_grades').click(function(){
		var input_reda = $('#input_reda').val();
		var input_human = $('#input_human').val();
		var input_nat = $('#input_nat').val();
		var input_ling = $('#input_ling').val();
		$.ajax({
			url: '/students/' + url_students_id + '/grades/'+ url_grades_id +'/edit/js/',
			data: {
				reda_js: input_reda,
				human_js: input_human,
				nat_js: input_nat,
				ling_js: input_ling
			},
			type: 'POST',
			success: function(response){
				console.log(response);
				window.location.href = ('/students/' + url_students_id + '/grades/?&students_id=' + url_students_id +'&students_name=' + url_name +'' );
			},
			error: function(error){
				console.log(error);
			}
		});
	});
});