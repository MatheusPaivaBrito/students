const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const urlsearch = urlParams.get('search')

$.ajax({
    url: '/students/search/js/',
    success: function(data) {

        $(function(){
            $('#button_students_search').click(function(){
        
                var search = document.getElementById('input_students_search').value;

                window.location.href = ('/students/search/?search=' + search)

            });
        });

        const fuse = new Fuse(data, {
            isCaseSensitive: true,
            minMatchCharLength: 2,
            keys: [
               'birth',
                'id',
                'name'
             ]
        });
        
        const result = fuse.search(urlsearch);

        for (var y=0; y<result.length ; y++) {
            for (var x=0; x<data.length; x++) {
                if (data[x].id == result[y].item.id) {
                    var students_id = data[x].id
                    var students = $( 
                        '<tbody>'
                        +
                            '<tr>'
                            +
                                '<td> '+ data[x].id +' </td>'    
                                +
                                '<td> ' +data[x].name +' </td>'
                                +
                                '<td> '+ moment.utc(data[x].birth).format('DD/MM/YYYY') +' </td>'   
                                +
                                '<td>'  
                                + 

                                    '<button name="button_grades" type="button" class="btn btn-outline-success" onclick="location.href='+"('/grades/"+data[x].id+"?&button="+x+""+"&id="+data[x].id+""+"&name="+data[x].name+""+"')"+'">'
                                        +
                                        '<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-journal-richtext" viewBox="0 0 20 20">'
                                        +
                                            '<path d="M7.5 3.75a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0zm-.861 1.542 1.33.886 1.854-1.855a.25.25 0 0 1 .289-.047L11 4.75V7a.5.5 0 0 1-.5.5h-5A.5.5 0 0 1 5 7v-.5s1.54-1.274 1.639-1.208zM5 9.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 0 1h-2a.5.5 0 0 1-.5-.5z"/>'
                                            +
                                            '<path d="M3 0h10a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2v-1h1v1a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H3a1 1 0 0 0-1 1v1H1V2a2 2 0 0 1 2-2z"/>'
                                            +
                                            '<path d="M1 5v-.5a.5.5 0 0 1 1 0V5h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1zm0 3v-.5a.5.5 0 0 1 1 0V8h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1zm0 3v-.5a.5.5 0 0 1 1 0v.5h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1z"/>'
                                            +
                                        '</svg>'
                                        +
                                        "Ver Notas" 
                                        + 
                                    '</button>'

                                    + 

                                    '<button name="button_delete" type="button" class="btn btn-outline-success" onclick="location.href='+"('/students/"+students_id+"/delete/')"+'">'
                                        +
                                        '<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-person-dash-fill" viewBox="0 0 20 20">'
                                            +
                                            '<path fill-rule="evenodd" d="M11 7.5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1h-4a.5.5 0 0 1-.5-.5z"/>'
                                            +
                                            '<path d="M1 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H1zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>'
                                            +
                                        '</svg>'
                                        +
                                        "Excluir"
                                        +  
                                    '</button>'

                                    +

                                    '<button name="button_edit" type="button" class="btn btn-outline-success" onclick="location.href='+"('/students/"+ data[x].id +"/edit/?&student_id="+ data[x].id +""+"')"+'">'
                                        +
                                        '<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-pen-fill" viewBox="0 0 20 20">'
                                            +
                                            '<path d="m13.498.795.149-.149a1.207 1.207 0 1 1 1.707 1.708l-.149.148a1.5 1.5 0 0 1-.059 2.059L4.854 14.854a.5.5 0 0 1-.233.131l-4 1a.5.5 0 0 1-.606-.606l1-4a.5.5 0 0 1 .131-.232l9.642-9.642a.5.5 0 0 0-.642.056L6.854 4.854a.5.5 0 1 1-.708-.708L9.44.854A1.5 1.5 0 0 1 11.5.796a1.5 1.5 0 0 1 1.998-.001z"/>'
                                            +
                                        '</svg>'
                                        +
                                        "Editar"  
                                        +
                                    '</button>'

                                + 
                                '</td>'
                            +
                            '</tr>'
                        +
                        '</tbody>'
                    ); 
                }
            $('#table_students').append(students);   
            }    
        }
    },
    error: function(jqXHR, textStatus, errorThrown){
        alert('Error: ' + textStatus + ' - ' + errorThrown);
    }
});