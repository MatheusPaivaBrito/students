function changepassworld() {
    var x = document.getElementById("input_password_signup");
    if (x.type === "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
  }

function changepassworldconfirm() {
    var x = document.getElementById("input_password_signup_confirm");
    if (x.type === "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
  }

$(function(){
	$('#button_submit_signup').click(function(){

		var username_signup = $('#input_username_signup').val();
        var username_signup_confirm = $('#input_username_signup_confirm').val();

		var password_signup = $('#input_password_signup').val();
        var password_signup_confirm = $('#input_password_signup_confirm').val();

        if (username_signup == username_signup_confirm && username_signup != "") {

            if (password_signup == password_signup_confirm && password_signup != "") {

                $.ajax({
                    url: '/signup/js/',
                    data: {
                        username_signup_js: username_signup,
                        password_signup_js: password_signup
                    },
                    type: 'POST',
                    success: function(data){
                        if (data == "The Username is Taken") {
                            alert("Nome de Usuario já está em Uso")
                        }else{
                            window.location.href = ('/');
                        }
                    },
                    error: function(error){
                        console.log(error);
                    }
                });
                
            } else {

                alert("Senhas Não Preenchidas ou Diferentes ");
                
            }
            
        } else {

            alert("Nomes de Usuario Não preenchidos ou Diferentes ");
            
        }

	});
});