const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const urlbutton = urlParams.get('button')
const urlid = urlParams.get('id')

$.ajax({
	url: '/patrimony/edit/js',
		success: function(data) {
			var student = data.length
				var students = $(
					'<h1 style="text-align: center;">' + "Editar Patrimônio de ID:" + urlid + '</h1>' 
							+
							'<div class="row">'
								+
								'<div class="col-md-6">'
								+
									'<input  type="text" name="name_patrimony" id="input_name_patrimony" placeholder="Nome" class="form-control" value="' + data[urlbutton].name_patrimony + '">'
								+
								'</div>'
								+
								'<div class="col-md-6">'
								+
									'<input  type="text" name="type_patrimony" id="input_type_patrimony" placeholder="Tipo" class="form-control" value="' + data[urlbutton].type_patrimony + '">'
								+
								'</div>'
								+
								'<div class="col-md-6">'
								+
									'<input  type="number" name="amount_patrimony" id="input_amount_patrimony" placeholder="Quantidade" class="form-control" value="' + data[urlbutton].amount_patrimony + '">'
								+
								'</div>'
								+
								'<div class="col-md-6">'
								+
									'<input  type="number" name="value_patrimony" id="input_value_patrimony" placeholder="Valor" class="form-control" value="' + data[urlbutton].value_patrimony + '">'
								+
								'</div>'
								+
							'</div>'
							+
						'<br>'
						+
						'<div class="d-grid gap-2 d-md-block">'
							+
							'<button id="editgradesbutton" type="button" class="btn btn-success">'
								+ 
								'<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-check-lg" viewBox="0 0 20 20">'
									+
									'<path d="M13.485 1.431a1.473 1.473 0 0 1 2.104 2.062l-7.84 9.801a1.473 1.473 0 0 1-2.12.04L.431 8.138a1.473 1.473 0 0 1 2.084-2.083l4.111 4.112 6.82-8.69a.486.486 0 0 1 .04-.045z"/>'
									+
								'</svg>'
								+
								"Salvar"
								+ 
							'</button>'
							+ 
						'</div>'
					);
				$('#patrimony_edit_form').append(students);				
			},
	error: function(jqXHR, textStatus, errorThrown){
		alert('Error: ' + textStatus + ' - ' + errorThrown);
	}
});

$(function(){
	$('#editgradesbutton').click(function(){
		var input_id_patrimony = urlid;
		var input_name_patrimony = $('#input_name_patrimony').val();
		var input_type_patrimony = $('#input_type_patrimony').val();
		var input_amount_patrimony = $('#input_amount_patrimony').val();
		var input_value_patrimony = $('#input_value_patrimony').val();
		$.ajax({
			url: '/patrimony/edit/js',
			data: {
				id_patrimony_js: input_id_patrimony,
				name_patrimony_js: input_name_patrimony,
				type_patrimony_js: input_type_patrimony,
				amount_patrimony_js: input_amount_patrimony,
				value_patrimony_js: input_value_patrimony
			},
			type: 'POST',
			success: function(response){
				console.log(response);
				window.location.href = ('/patrimony');
			},
			error: function(error){
				console.log(error);
			}
		});
	});
});