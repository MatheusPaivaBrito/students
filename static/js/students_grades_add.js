const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const url_id = urlParams.get('students_id')

$(function(){
	$('#button_add_grades').click(function(){
		var input_reda = $('#input_reda').val();
		var input_human = $('#input_human').val();
		var input_nat = $('#input_nat').val();
		var input_ling = $('#input_ling').val();
		$.ajax({
			url: '/students/' + url_id + '/grades/add/js/',
			data: {
				owner_id_js: url_id,
				reda_js: input_reda,
				human_js: input_human,
				nat_js: input_nat,
				ling_js: input_ling,
			},
			type: 'POST',
			success: function(){
				window.location.href = ('/students/' + url_id + '/grades/' + '?&parameters=' + urlParams);
			},
			error: function(){
			}
		});
	});
});