$(function(){
	$('#add_patrimony_button').click(function(){
		var input_name_patrimony = $('#input_name_patrimony').val();
		var input_type_patrimony = $('#input_type_patrimony').val();
        var input_amount_patrimony = $('#input_amount_patrimony').val();
		var input_value_patrimony = $('#input_value_patrimony').val();
		$.ajax({
			url: '/patrimony/add/js',
			data: {
				name_patrimony_js: input_name_patrimony,
				type_patrimony_js: input_type_patrimony,
                amount_patrimony_js: input_amount_patrimony,
                value_patrimony_js: input_value_patrimony
			},
			type: 'POST',
			success: function(response){
				console.log(response);
				window.location.href = ('/patrimony');
			},
			error: function(error){
				console.log(error);
			}
		});
	});
});