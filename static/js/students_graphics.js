$.ajax({
    url: '/students/graphics/js/',
    type: 'POST',
    success: function(data){

        const students = data
    
        const name = students.map(x => x.name);
        const avarage = students.map(y => y.avarage);

        console.log(data[0].name)

        var ctx = document.getElementById('myChart').getContext('2d');

        function getLineData(data) {
            var data = []
          
            for (var x = 0; x < students.length; x++) {
                for (var y = 0; y < notes.length; y++) {
                    if (students[x].id == notes[y].owner_id) {
                        
                        data.push(notes[y].avarage)

                    }
                    
                }
                data.push(null)

            }
          
            return data
        }

        function getLinelabels(data) {
            var labels = []
          
            for(var x=0; x < students.length; x++) {
                labels.push(students[x].name)
            }
          
            return labels
        }

        function backgroundColor(data) {
            var backgroundColor = []
          
            for(var BgC=0; BgC < students.length; BgC++) {
                var dynamicColors = function() {
                    var r = Math.floor(Math.random() * 255);
                    var g = Math.floor(Math.random() * 255);
                    var b = Math.floor(Math.random() * 255);
                    return "rgba(" + r + "," + g + "," + b + "," + 0.5 + ")";
                 };
        
                 backgroundColor.push(dynamicColors())
            }
          
            return backgroundColor
        }

        function borderColor(data) {
            var borderColor = []
          
            for(var BC=0; BC < students.length; BC++) {
                borderColor.push('rgba(255, 99, 132, 0.5)')
            }
          
            return borderColor
        }

            var myChart = new Chart(ctx, {

                data: {

                    labels: name,
    
                    datasets: [{
                        type: 'bar',
                        label: [],
                        data: avarage,
                        backgroundColor: backgroundColor(50, 4),
                        borderColor: borderColor(50, 4),
                        borderWidth: 1
                    }],
        
                    
                },
                    
                options: {
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    }
                }

            }); 
               
    },
});
