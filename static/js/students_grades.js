const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const urlid = urlParams.get('students_id')
const urlname = urlParams.get('students_name')

$.ajax({
    url: '/students/' + urlid + '/grades/js/',
    success: function(data) {

		var header = $(
		'<h1 style="text-align: center;">Notas de '+ urlname +'</h1>'
			+

			'<button onclick="location.href='+"('/students/' + urlid + '/grades/add/?parameters=" + queryString + "" +"')"+'"   type="button" type=button class="btn btn-blick btn-success">'
			+
				'<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-journal-plus" viewBox="0 0 20 20">'
				+
					'<path fill-rule="evenodd" d="M8 5.5a.5.5 0 0 1 .5.5v1.5H10a.5.5 0 0 1 0 1H8.5V10a.5.5 0 0 1-1 0V8.5H6a.5.5 0 0 1 0-1h1.5V6a.5.5 0 0 1 .5-.5z"/>'
					+
					'<path d="M3 0h10a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2v-1h1v1a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H3a1 1 0 0 0-1 1v1H1V2a2 2 0 0 1 2-2z"/>'
					+
					'<path d="M1 5v-.5a.5.5 0 0 1 1 0V5h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1zm0 3v-.5a.5.5 0 0 1 1 0V8h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1zm0 3v-.5a.5.5 0 0 1 1 0v.5h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1z"/>'
					+
				'</svg>'
			+
				"Adicionar Notas"
			+
			'</button>'

			+

			'<button id="Return" onclick=" location.href = '+ "('/students')" +'  "  type="button" type=button class="btn btn-blick btn-success">'
			+
				'<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-people-fill" viewBox="0 0 20 20">'
				+
					'<path d="M7 14s-1 0-1-1 1-4 5-4 5 3 5 4-1 1-1 1H7zm4-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>'
					+
					'<path fill-rule="evenodd" d="M5.216 14A2.238 2.238 0 0 1 5 13c0-1.355.68-2.75 1.936-3.72A6.325 6.325 0 0 0 5 9c-4 0-5 3-5 4s1 1 1 1h4.216z"/>'
					+
					'<path d="M4.5 8a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5z"/>'
					+
				'</svg>'
			+
				"Retornar a Alunos"
			+
			'</button>'
			+

			'<select onchange="changefilterfunction()" name="gradesselect" id="select_filter" type=button class="btn btn-blick btn-success">'
				+
				'<option value="1">Sem Filtro</option>'
				+
				'<option value="2">Ordem Crescente</option>'
				+
				'<option value="3">Ordem Decrescente</option>'
			+
            '</select>'

		);

		$('#notes').append(header);

        for (var x=0; x<data.length; x++) {
				var grades = $(
					'<tbody>'
					+
						'<tr>'
						+
							'<td> ' + data[x].reda + ' </td>'    
							+
							'<td> ' + data[x].human + ' </td>'
							+
							'<td> ' + data[x].nat + ' </td>'   
							+
							'<td> ' + data[x].ling + ' </td>'   
							+
							'<td> ' + data[x].avarage + ' </td>'
							+
							'<td>'  
							+ 
								'<button id="buton_delete_notes" type="button" type=button class="btn btn-outline-success" onclick="location.href='+"('/students/"+ urlid +"/grades/"+ data[x].id +"/delete/?&students_id=" + urlid + "&students_name=" + urlname + " ')"+'" >'
								+
								'<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-journal-minus" viewBox="0 0 20 20">'
								+
									'<path fill-rule="evenodd" d="M5.5 8a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1H6a.5.5 0 0 1-.5-.5z"/>'
									+
									'<path d="M3 0h10a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2v-1h1v1a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H3a1 1 0 0 0-1 1v1H1V2a2 2 0 0 1 2-2z"/>'
									+
									'<path d="M1 5v-.5a.5.5 0 0 1 1 0V5h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1zm0 3v-.5a.5.5 0 0 1 1 0V8h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1zm0 3v-.5a.5.5 0 0 1 1 0v.5h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1z"/>'
									+
								'</svg>'
								+
								"Excluir" 
								+
								'</button>'
								+
								'<button id="buton_edit_notes" type="button" type=button class="btn btn-outline-success" onclick="location.href='+"('/students/"+ urlid +"/grades/"+ data[x].id +"/edit/?&students_id=" + urlid + "&grades_id=" + data[x].id + "&name=" + urlname + "')"+'" >'
								+ 
								'<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-pen-fill" viewBox="0 0 20 20">'
									+
									'<path d="m13.498.795.149-.149a1.207 1.207 0 1 1 1.707 1.708l-.149.148a1.5 1.5 0 0 1-.059 2.059L4.854 14.854a.5.5 0 0 1-.233.131l-4 1a.5.5 0 0 1-.606-.606l1-4a.5.5 0 0 1 .131-.232l9.642-9.642a.5.5 0 0 0-.642.056L6.854 4.854a.5.5 0 1 1-.708-.708L9.44.854A1.5 1.5 0 0 1 11.5.796a1.5 1.5 0 0 1 1.998-.001z"/>'
									+
								'</svg>'
								+
								"Editar" 
								+  
								'</button>'
							+ 
							'</td>'
						+
						'</tr>'
					+
					'</tbody>'
				);
          $('#table_notes').append(grades);
        }
    },
    error: function(jqXHR, textStatus, errorThrown){
        alert('Error: ' + textStatus + ' - ' + errorThrown);
    }
});

function changefilterfunction(){
	var filterselect = document.getElementById("select_filter").value;
		$.ajax({
		url: '/students/' + urlid + '/grades/js/',
		type: 'POST',
		success: function(response){
			window.location.href = ('/students/' + urlid + '/grades/filter/' + filterselect + '/' + '?&id=' + urlid +'&name=' + urlname + '&filter=' + filterselect);
									
		},
		error: function(error){
			console.log(error);
		}
	});
}