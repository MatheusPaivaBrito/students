$(function(){
	$('#button_add_students').click(function(){
		var input_name_students = $('#input_name_students').val();
		var input_birth_students = $('#input_birth_students').val();
		$.ajax({
			url: '/students/add/js/',
			data: {
				name_students_js: input_name_students,
				birth_students_js: input_birth_students
			},
			type: 'POST',
			success: function(response){
				console.log(response);
				window.location.href = ('/students/');
			},
			error: function(error){
				console.log(error);
			}
		});
	});
});