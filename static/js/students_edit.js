const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const urlid = urlParams.get('student_id')

$.ajax({
	url: '/students/'+urlid+'/edit/js/',
		success: function(data) {
			var student = data.length
				var students = $(
					'<h1 style="text-align: center;">' + "Editar "  + data[0].name + '</h1>'
							+
							'<div class="row">'
								+
								'<div class="col-md-6">'
								+
									'<input id="input_name_students" name="name" type="text" class="form-control" placeholder="Nome" value="' + data[0].name + '">'
								+
								'</div>'
								+
								'<div class="col-md-6">'
								+									
									'<input id="input_birth_students" name="birth" type="date" class="form-control" placeholder="Data de Nascimento" value="' + moment.utc(data[0].birth).format('YYYY-MM-DD') + '">'
								+
								'</div>'
								+
							'</div>'
							+
						'<br>'
						+
						'<div class="d-grid gap-2 d-md-block">'
							+

							'<button id="button_edit_students" type="button" class="btn btn-success">'
							+ 
							'<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-check-lg" viewBox="0 0 20 20">'
								+
								'<path d="M13.485 1.431a1.473 1.473 0 0 1 2.104 2.062l-7.84 9.801a1.473 1.473 0 0 1-2.12.04L.431 8.138a1.473 1.473 0 0 1 2.084-2.083l4.111 4.112 6.82-8.69a.486.486 0 0 1 .04-.045z"/>'
								+
							'</svg>'
							+
							"Salvar"
							+ 
							'</button>'
							+ 

					'</div>'
					);
				$('#form_edit_students').append(students);				
			},
	error: function(jqXHR, textStatus, errorThrown){
		alert('Error: ' + textStatus + ' - ' + errorThrown);
	}
});

$(function(){
	$('#button_edit_students').click(function(){
		var input_name_students = $('#input_name_students').val();
		var input_birth_students = $('#input_birth_students').val();
		$.ajax({
			url: '/students/'+urlid+'/edit/js/',
			data: {
				name_students_js: input_name_students,
				birth_students_js: input_birth_students
			},
			type: 'POST',
			success: function(response){
				console.log(response);
				window.location.href = ('/students/');
			},
			error: function(error){
				console.log(error);
			}
		});
	});
});