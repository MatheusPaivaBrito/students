const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const urlid = urlParams.get('id')
const urlname = urlParams.get('name')
const urlfilter = urlParams.get('filter')

$.ajax({
    url: '/students/' + urlid + '/grades/filter/' + urlfilter + '/js/',
    success: function(data) {
		if (urlfilter == 1) {
			var header = $(
				'<h1 style="text-align: center;">Notas de '+ urlname +'</h1>'
					+
					'<button onclick="location.href='+"('/addgrades/"+urlid+"?parameters="+queryString+"" +"')"+'"   type="button" type=button class="btn btn-blick btn-success"> Adicionar Notas  </button>'
					+
					'<button onclick=" location.href = '+ "('/students')" +'  "  type="button" type=button class="btn btn-blick btn-success"> Retornar a Alunos  </button>'
					+
					'<select id="select_filter" name="select_grades" type=button class="btn btn-blick btn-success" onchange="changefilterfunction()">'
					+
					'<option value="1" selected > Sem Filtro </option>'
					+
					'<option value="2"> Ordem Crescente </option>'
					+
					'<option value="3"> Ordem Decrescente </option>'
					+
					'</select>'
				);
			} else {
				if (urlfilter == 2) {
					var header = $(
						'<h1 style="text-align: center;">Notas de '+ urlname +'</h1>'
							+
							'<button onclick="location.href='+"('/addgrades/"+urlid+"?parameters="+queryString+"" +"')"+'"   type="button" type=button class="btn btn-blick btn-success"> Adicionar Notas  </button>'
							+
							'<button onclick=" location.href = '+ "('/students')" +'  "  type="button" type=button class="btn btn-blick btn-success"> Retornar a Alunos  </button>'
							+
							'<select id="select_filter" name="select_grades" type=button class="btn btn-blick btn-success" onchange="changefilterfunction()">'
							+
							'<option value="1"> Sem Filtro </option>'
							+
							'<option value="2" selected > Ordem Crescente </option>'
							+
							'<option value="3"> Ordem Decrescente </option>'
							+
							'</select>'
						);
					} else {
						if (urlfilter == 3) {
							var header = $(
								'<h1 style="text-align: center;">Notas de '+ urlname +'</h1>'
									+
									'<button onclick="location.href='+"('/addgrades/"+urlid+"?parameters="+queryString+"" +"')"+'"   type="button" type=button class="btn btn-blick btn-success"> Adicionar Notas  </button>'
									+
									'<button onclick=" location.href = '+ "('/students')" +'  "  type="button" type=button class="btn btn-blick btn-success"> Retornar a Alunos  </button>'
									+
									'<select id="select_filter" name="select_grades" type=button class="btn btn-blick btn-success" onchange="changefilterfunction()">'
									+
									'<option value="1"> Sem Filtro </option>'
									+
									'<option value="2"> Ordem Crescente </option>'
									+
									'<option value="3"selected> Ordem Decrescente< /option>'
									+
									'</select>'
									);
								}
							}
						}

		$('#div_filter_notes').append(header);

        for (var x=0; x<data.length; x++) {
			if (data[x].owner_id == urlid) {
				var students = $( 
					'<tbody>'
					+
						'<tr>'
						+
							'<td> ' + data[x].reda + ' </td>'    
							+
							'<td> ' + data[x].human + ' </td>'
							+
							'<td> ' + data[x].nat + ' </td>'   
							+
							'<td> ' + data[x].ling + ' </td>'   
							+
							'<td> ' + data[x].avarage + ' </td>'
							+
							'<td>'  
							+ 
								'<button id="buton_notes_delete" type="button" type=button class="btn btn-outline-success" onclick="location.href='+"('/students/"+ urlid +"/grades/"+ data[x].id +"/delete/?&students_id=" + urlid + "&students_name=" + urlname + " ')"+'"> Excluir  </button>'
								+
								'<button type="button" type=button class="btn btn-outline-success" onclick="location.href='+"('/students/"+ urlid +"/grades/"+ data[x].id +"/edit/?&students_id=" + urlid + "&grades_id=" + data[x].id + "&name=" + urlname + "')"+'"> Editar  </button>'
							+ 
							'</td>'
						+
						'</tr>'
					+
					'</tbody>'
				);
			}
          $('#table_filter_notes').append(students);
        }
    },
    error: function(jqXHR, textStatus, errorThrown){
        alert('Error: ' + textStatus + ' - ' + errorThrown);
    }
});

function changefilterfunction(){
	var filterselect = document.getElementById("select_filter").value;
		$.ajax({
		url: '/students/' + urlid + '/grades/filter/' + urlfilter + '/js/',
		type: 'POST',
		success: function(response){
			window.location.href = ('/students/' + urlid + '/grades/filter/' + filterselect + '/' + '?&id=' + urlid +'&name=' + urlname + '&filter=' + filterselect);
		},
		error: function(error){
			console.log(error);
		}
	});
}