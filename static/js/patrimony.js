$.ajax({
    url: '/patrimony/js',
    success: function(data) {

        $(function(){
            $('#searchbutton').click(function(){
        
                var search = document.getElementById('searchinput').value;

                window.location.href = ('/index/search?search=' + search)

            });
        });

        for (var x=0; x<data.length; x++) {

            var patrimony = $( 
            '<tbody>'
                +
                '<tr>'
                +
                    '<td> '+ data[x].name_patrimony +' </td>'    
                +
                    '<td> '+ data[x].type_patrimony +' </td>'
                +
                    '<td> '+ data[x].amount_patrimony +' </td>'   
                +
                    '<td> '+ data[x].value_patrimony +' </td>'  
                +
                    '<td>'  
                    +  
                        '<button onclick=" location.href = '+ "('/patrimony/delete/"+ data[x].id_patrimony + "')" +'  "  type="button" class="btn btn-outline-success">'
                        +
                        '<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-person-dash-fill" viewBox="0 0 20 20">'
                            +
                            '<path fill-rule="evenodd" d="M11 7.5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1h-4a.5.5 0 0 1-.5-.5z"/>'
                            +
                            '<path d="M1 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H1zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>'
                            +
                        '</svg>'
                        +
                            "Excluir"
                        +  
                        '</button>'
                        +
                        '<button onclick="location.href='+"('patrimony/edit/"+data[x].id_patrimony+"?&button="+x+""+"&id="+data[x].id_patrimony+"" +"')"+'"  type="button" class="btn btn-outline-success">'
                        +
                        '<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-pen-fill" viewBox="0 0 20 20">'
                            +
                            '<path d="m13.498.795.149-.149a1.207 1.207 0 1 1 1.707 1.708l-.149.148a1.5 1.5 0 0 1-.059 2.059L4.854 14.854a.5.5 0 0 1-.233.131l-4 1a.5.5 0 0 1-.606-.606l1-4a.5.5 0 0 1 .131-.232l9.642-9.642a.5.5 0 0 0-.642.056L6.854 4.854a.5.5 0 1 1-.708-.708L9.44.854A1.5 1.5 0 0 1 11.5.796a1.5 1.5 0 0 1 1.998-.001z"/>'
                            +
                        '</svg>'
                        +
                            "Editar"  
                        +
                        '</button>'
                    + 
                    '</td>'
                +
                '</tr>'
            +
            '</tbody>'
            );

            $('#patrimony_table').append(patrimony);

        }
    },
    error: function(jqXHR, textStatus, errorThrown){
        alert('Error: ' + textStatus + ' - ' + errorThrown);
    }
});